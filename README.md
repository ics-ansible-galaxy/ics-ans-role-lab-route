# ics-ans-role-lab-route

Ansible role to add static routes to GPN machines.
This role adds a route to the networks defined in `lab_route_networks` on the following networks:

- 10.0.42.0 (gw: 10.0.42.2)
- 194.47.240.0 (gw: 194.47.240.2) 

## Role Variables

```yaml
lab_route_networks:
  - 172.16.0.0/16
  - 172.17.0.0/16
  - 172.18.0.0/16
  - 172.19.0.0/16
  - 172.20.0.0/16
  - 172.30.0.0/16
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-lab-route
```

## License

BSD 2-clause
