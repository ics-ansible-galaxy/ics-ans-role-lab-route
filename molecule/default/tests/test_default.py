import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_route_interfaces(host):
    file = host.file("/etc/sysconfig/network-scripts/route-eth0")
    assert file.contains("172.30.0.0")


def test_route_exist(host):
    cmd = host.run("/usr/sbin/ip route list")
    assert "172.30.0.0" in cmd.stdout
